import { Props } from './props';

const colorsBase = {
  accentBlue: 'blue',
  accentBlueHover: '#0000af',
  blockShadow: '0 37px 26px -25px rgba(0,0,0,.3)',
};

export const colorsLight: Props = {
  ...colorsBase,
  theme: 'light',
  globalBg: '#fff',
  darkBg: '#efefef',
  textColor: '#282828',
};

export const colors: Props = {
  ...colorsBase,
  theme: 'dark',
  globalBg: '#060815',
  darkBg: '#0c0f1f',
  textColor: '#fff',
};
