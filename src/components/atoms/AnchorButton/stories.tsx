import { useState } from '@storybook/addons';
import classNames from 'classnames';
import React, { FC } from 'react';

import { Anchor } from '../Anchor';
import { Container } from '../Container';
import { AnchorButton } from './component';

export default {
  title: 'Atoms/AnchorButton',
};

export const AnchorButtonBasic: FC = () => {
  const [count, setCount] = useState<number>(0);
  function clickHandler(): void {
    setCount(count + 1);
  }
  return (
    <>
      <Container mini>
        <Anchor href="#" className={classNames('d-block', 'mb-2')}>
          This is an actual link.
        </Anchor>
        <AnchorButton onClick={clickHandler} className={classNames('d-block', 'mb-2')}>
          This is Button Link. Handled {count}
        </AnchorButton>
      </Container>
    </>
  );
};
