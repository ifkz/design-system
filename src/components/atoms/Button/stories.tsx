import styled from '@emotion/styled';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useState } from '@storybook/addons';
import classNames from 'classnames';
import React, { FC } from 'react';

import { colors } from '../../../core/colors';
import { Container } from '../Container';
import { Button } from './component';

export default {
  title: 'Atoms/Button',
};

const Responser = styled.div`
  background: ${colors.darkBg};
  border-radius: 10px;
  height: 50px;
  color: #fff;
`;

export const ButtonBasic: FC = () => {
  const [count, setCount] = useState<number>(0);
  function clickHandler(): void {
    setCount(count + 1);
  }

  return (
    <>
      <Container mini className={classNames('my-4', 'text-center')}>
        <Button colors={colors} onClick={clickHandler}>
          Click to trigger
        </Button>
        <Responser className={classNames('d-block', 'mt-2', 'p-3')}>
          Button clicked {count} times
        </Responser>
      </Container>
    </>
  );
};

export const ButtonIconed: FC = () => {
  const [count, setCount] = useState<number>(0);
  function clickHandler(): void {
    setCount(count + 1);
  }

  return (
    <>
      <Container mini className={classNames('my-4', 'text-center')}>
        <Button
          colors={colors}
          onClick={clickHandler}
          icon={<FontAwesomeIcon icon={faArrowRight} />}
        >
          Click to trigger
        </Button>
        <Responser className={classNames('d-block', 'mt-2', 'p-3')}>
          Button clicked {count} times
        </Responser>
      </Container>
    </>
  );
};
