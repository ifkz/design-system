import React, { FC, useState } from 'react';

import { colors, colorsLight } from '../../../core';
import { Switch } from './component';

export default {
  title: 'Atoms/Switch',
};

export const SwitchBasic: FC = () => {
  const [isActive, setIsActive] = useState(false);
  const [isActive2, setIsActive2] = useState(false);

  function switchHandler(): void {
    setIsActive(!isActive);
  }
  function switchHandler2(): void {
    setIsActive2(!isActive2);
  }

  return (
    <>
      <div className="container">
        <Switch colors={colors} className="mb-2" isActive={isActive} onClick={switchHandler} />
        <Switch
          className="mb-2"
          colors={colorsLight}
          isActive={isActive2}
          onClick={switchHandler2}
        />
      </div>
    </>
  );
};
