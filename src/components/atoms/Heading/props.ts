import { HTMLAttributes } from 'react';

import { Elements } from './types/headings';

export type Props = HTMLAttributes<HTMLHeadingElement> & {
  readonly as: keyof Elements;
  readonly color?: string;
};
