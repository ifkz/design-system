import styled from '@emotion/styled';
import React, { FC } from 'react';

import { Props } from './props';

const LocaleSelectBase: FC<Props> = ({ languages, locale, onChange, ...rest }: Props) => {
  return (
    <div {...rest}>
      <div className="switcher--container">
        <div className="switcher">
          {Object.keys(languages).map((language, i) => (
            <label key={i}>
              <input
                type="radio"
                name="locale"
                value={language}
                onChange={onChange}
                checked={locale === language}
                hidden
              />
              <div className="switcher__item">
                <img alt="" className="switcher__item--img" src={languages[language]} />
                {language}
              </div>
            </label>
          ))}
        </div>
      </div>
    </div>
  );
};

export const LocaleSelect = styled(LocaleSelectBase)`
  .switcher {
    margin-top: 0;
    display: block;
    background: rgba(255, 255, 255, 0.04);
    padding: 5px;
    border-radius: 22px;
    margin-bottom: 0;
  }
  .switcher__item {
    cursor: pointer;
    color: #fff !important;
    display: flex;
    align-items: center;
    border-radius: 100px;
    padding: 10px 15px;
    font-weight: 600;
    text-transform: uppercase;
    font-size: 12px;
  }
  .switcher__item--img {
    height: 11px;
    width: 15px;
    margin-right: 5px;
    display: block;
  }
  input:checked + .switcher__item {
    color: #fff;
    background: rgba(255, 255, 255, 0.2);
  }
`;
