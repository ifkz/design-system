import styled from '@emotion/styled';
import React, { FC } from 'react';

import { Anchor } from '../../../../atoms/Anchor';
import { Props } from './props';

const ItemBase: FC<Props> = ({ children, colors: _a, ...rest }: Props) => (
  <Anchor {...rest}>{children}</Anchor>
);

export const Item = styled(ItemBase)<Pick<Props, 'colors'>>`
  text-decoration: none;
  color: #fff;
  font-weight: 600;
  text-transform: uppercase;
  font-size: 20px;
  padding: 5px 0;
  transition: color 0.2s, transform 0.2s;
  &:hover {
    color: ${({ colors }) => colors.accentBlue};
    transform: translateX(-10px);
  }
`;
