import styled from '@emotion/styled';
import React, { FC } from 'react';

import { Props } from './props';

const NavigationBase: FC<Props> = ({ children, ...rest }: Props) => (
  <div className="mobile-navigation" {...rest}>
    {children}
  </div>
);

export const Navigation = styled(NavigationBase)`
  position: fixed;
  right: 0;
  top: 0;
  display: flex;
  flex-direction: column;
  background: #0e162b;
  padding: 100px 50px;
  height: 100vh;
  z-index: 9999;
  box-shadow: 0 16px 23px -10px rgba(0, 0, 0, 0.3);
  overflow-y: scroll;
  overflow-x: hidden;
`;
