import { ItemProps } from './libs/Item';
import { Props } from './props';

export * from './component';
export { Item as NavigationItem } from './libs/Item';
export type NavigationProps = Props;
export type NavigationItemProps = ItemProps;
