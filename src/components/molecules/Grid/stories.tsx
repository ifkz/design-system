import React, { FC } from 'react';

import { Container } from '../../atoms/Container';
import { MockBackground } from '../../atoms/MockBackground';
import { Grid } from './component';
import { Item } from './libs/Item';

export default {
  title: 'Molecules/Grid',
};

export const GridBasic: FC = () => (
  <>
    <Container>
      <Grid className="mb-4" columns={2}>
        <MockBackground className="text-center">1</MockBackground>
        <MockBackground className="text-center">2</MockBackground>
        <MockBackground className="text-center">3</MockBackground>
        <MockBackground className="text-center">4</MockBackground>
      </Grid>
      <Grid className="mb-4" columns={2} gap="5px">
        <MockBackground className="text-center">1</MockBackground>
        <MockBackground className="text-center">2</MockBackground>
        <MockBackground className="text-center">3</MockBackground>
        <MockBackground className="text-center">4</MockBackground>
      </Grid>
      <Grid className="mb-4" columns={3} gap="5px">
        <MockBackground className="text-center">1</MockBackground>
        <MockBackground className="text-center">2</MockBackground>
        <MockBackground className="text-center">3</MockBackground>
        <MockBackground className="text-center">4</MockBackground>
        <MockBackground className="text-center">5</MockBackground>
        <MockBackground className="text-center">6</MockBackground>
      </Grid>
    </Container>
  </>
);

export const GridFeed: FC = () => (
  <>
    <Container>
      <Grid className="mb-4" columns={12} gap="20px">
        <Item columns={12}>
          <MockBackground className="text-center">1</MockBackground>
        </Item>
        <Item columns={12} rowColumns={3}>
          <MockBackground className="text-center">2</MockBackground>
        </Item>
        <Item columns={12} rowColumns={3}>
          <MockBackground className="text-center">3</MockBackground>
        </Item>
        <Item columns={12} rowColumns={3}>
          <MockBackground className="text-center">4</MockBackground>
        </Item>
        <Item columns={12} rowColumns={4}>
          <MockBackground className="text-center">5</MockBackground>
        </Item>
        <Item columns={12} rowColumns={4}>
          <MockBackground className="text-center">6</MockBackground>
        </Item>
        <Item columns={12} rowColumns={4}>
          <MockBackground className="text-center">7</MockBackground>
        </Item>
        <Item columns={12} rowColumns={4}>
          <MockBackground className="text-center">8</MockBackground>
        </Item>
      </Grid>
    </Container>
  </>
);
