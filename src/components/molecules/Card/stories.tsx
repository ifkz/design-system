import classNames from 'classnames';
import React, { FC } from 'react';

import { colors, colorsLight } from '../../../core/colors';
import { Container } from '../../atoms/Container';
import { Heading } from '../../atoms/Heading';
import { Card } from './component';

export default {
  title: 'Molecules/Card',
};

export const CardBasic: FC = () => (
  <>
    <Container>
      <Card className={classNames('mb-2')} colors={colors}>
        <Heading as="h3">Dark theme Card</Heading>
      </Card>
      <Card className={classNames('mb-2')} colors={colorsLight}>
        <Heading as="h3">Light theme Card</Heading>
      </Card>
    </Container>
  </>
);
