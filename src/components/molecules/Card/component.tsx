import styled from '@emotion/styled';

import { Props } from './props';

export const Card = styled.div<Props>`
  ${({ colors, border }) => `
    padding: 20px;
    border-radius: 10px;
    background: ${colors.darkBg};
    color: ${colors.textColor};
    overflow: hidden;
    ${border && 'border: 1px solid rgba(0,0,0,.1);'}
  `}
`;
