import { AuthorProps } from './libs/Author';
import { BodyProps } from './libs/Body';
import { CoverProps } from './libs/Cover';
import { PublishedProps } from './libs/Published';
import { Props } from './props';

export * from './component';

export { Cover as PostCardCover } from './libs/Cover';
export { Body as PostCardBody } from './libs/Body';
export { Author as PostCardAuthor } from './libs/Author';
export { Published as PostCardPublished } from './libs/Published';

export type PostCardProps = Props;
export type PostCardCoverProps = CoverProps;
export type PostCardBodyProps = BodyProps;
export type PostCardAuthorProps = AuthorProps;
export type PostCardPublishedProps = PublishedProps;
