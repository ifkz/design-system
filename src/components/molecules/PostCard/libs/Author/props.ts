import { AnchorProps } from '../../../../atoms/Anchor';
import { AnchorButtonProps } from '../../../../atoms/AnchorButton';

export type Props = AnchorButtonProps | AnchorProps;
