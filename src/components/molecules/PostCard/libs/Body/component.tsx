import styled from '@emotion/styled';

export const Body = styled.div`
  flex-grow: 1;
  padding: 40px 40px 60px;
`;
