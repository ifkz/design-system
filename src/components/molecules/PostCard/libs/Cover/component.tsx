import styled from '@emotion/styled';

import { Props } from './props';

export const Cover = styled.div<Props>`
  background-image: url(${({ url }) => url});
  background-size: cover;
  background-position: center;
  text-align: center;
  width: 100%;
  padding-top: 56.25%;
  background-color: darkgrey;
`;
