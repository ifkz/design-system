import { ButtonHTMLAttributes, HTMLAttributes } from 'react';

import { ColorProps } from '../../../core/colors';

export type Props = Omit<HTMLAttributes<HTMLDivElement>, 'onClick'> &
  Pick<ButtonHTMLAttributes<HTMLButtonElement>, 'onClick'> & {
    readonly isActive: boolean;
    readonly colors: ColorProps;
  };
