import { FC, useState } from 'react';
import * as React from 'react';

import { colors, colorsLight } from '../../../core';
import { Container } from '../../atoms/Container';
import { MockBackground } from '../../atoms/MockBackground';
import { ThemeSwitch } from './component';

export default {
  title: 'Organisms/Header',
};

export const ThemeSwitchBasic: FC = () => {
  const [theme, setTheme] = useState(false);
  function switchTheme(): void {
    setTheme(!theme);
  }
  return (
    <>
      <Container mini>
        <MockBackground className="mb-3" background={colors.darkBg}>
          <ThemeSwitch colors={colors} isActive={theme} onClick={switchTheme}>
            Theme
          </ThemeSwitch>
        </MockBackground>
        <MockBackground className="mb-3" background={colors.darkBg}>
          <ThemeSwitch colors={colorsLight} isActive={theme} onClick={switchTheme}>
            Theme
          </ThemeSwitch>
        </MockBackground>
      </Container>
    </>
  );
};
