import React, { FC } from 'react';

import { colors } from '../../../core/colors';
import { Container } from '../../atoms/Container';
import { MockBackground } from '../../atoms/MockBackground';
import { Butter } from './component';

export default {
  title: 'Molecules/Butter',
};

export const ButterBasic: FC = () => (
  <>
    <Container mini>
      <MockBackground background={colors.darkBg}>
        <Butter colors={colors}>Menu</Butter>
      </MockBackground>
    </Container>
  </>
);
