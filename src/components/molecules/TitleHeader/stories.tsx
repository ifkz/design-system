/** @jsx jsx */
import { jsx } from '@emotion/core';
import { faAngry } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FC } from 'react';

import { colors, colorsLight } from '../../../core/colors';
import { Container } from '../../atoms/Container';
import { Heading } from '../../atoms/Heading';
import { TitleHeader } from './component';

export default {
  title: 'Molecules/TitleHeader',
};

export const DarkTheme: FC = () => (
  <React.Fragment>
    <Container mini>
      <TitleHeader className="mb-2" colors={colors} subtitle="7 записей">
        <Heading className="m-0" as="h3">
          Наши кейсы
        </Heading>
      </TitleHeader>
      <TitleHeader
        className="mb-2"
        colors={colors}
        subtitle={
          <FontAwesomeIcon icon={faAngry} color="yellow" css={{ color: 'yellow', fontSize: 25 }} />
        }
      >
        <Heading className="m-0" as="h3">
          Use any node as a subtitle
        </Heading>
      </TitleHeader>
    </Container>
  </React.Fragment>
);

export const LightTheme: FC = () => (
  <React.Fragment>
    <Container mini>
      <TitleHeader className="mb-2" colors={colorsLight} subtitle="7 записей">
        <Heading className="m-0" as="h3">
          Наши кейсы
        </Heading>
      </TitleHeader>
      <TitleHeader
        className="mb-2"
        colors={colorsLight}
        subtitle={
          <FontAwesomeIcon icon={faAngry} color="yellow" css={{ color: '#FFC107', fontSize: 25 }} />
        }
      >
        <Heading className="m-0" as="h3">
          Use any node as a subtitle
        </Heading>
      </TitleHeader>
    </Container>
  </React.Fragment>
);
