import React, { FC } from 'react';

import { colors } from '../../../core/colors';
import { MockBackground } from '../../atoms/MockBackground';
import { FooterAbout } from './component';

export default {
  title: 'Organisms/Footer',
};

export const FooterAboutBasic: FC = () => (
  <>
    <MockBackground background={colors.globalBg}>
      <FooterAbout title="О проекте">
        Проект разработан при финансовой поддержке Фонда Сорос-Казахстан. Содержание любой
        публикации отражает точку зрения автора/ов, которая не обязательно совпадает с точкой зрения
        Фонда Сорос-Казахстан.
      </FooterAbout>
    </MockBackground>
  </>
);
