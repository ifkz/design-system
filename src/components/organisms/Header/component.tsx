import styled from '@emotion/styled';
import classNames from 'classnames';
import React, { FC } from 'react';

import { Container } from '../../atoms/Container';
import { Props } from './props';

const HeaderBase: FC<Props> = ({ className, children, colors: _a, ...rest }: Props) => {
  return (
    <header className={classNames(className, 'header', 'component-navigation')} {...rest}>
      <Container className={classNames('d-flex', 'align-items-center', 'justify-content-between')}>
        {children}
      </Container>
    </header>
  );
};

export const Header = styled(HeaderBase)<Pick<Props, 'colors'>>`
  top: 0;
  width: 100%;
  padding: 10px 0;
  z-index: 999;
  ${({ colors }) =>
    colors.theme === 'light' &&
    `& .logo-title {
      color: #282828!important;
    }`}
  .shade {
    position: fixed;
    width: 100%;
    height: 100%;
    background: rgba(11, 9, 34, 0.8);
    top: 0;
    left: 0;
    z-index: -2;
    outline: none;
    border: none;
  }
`;
