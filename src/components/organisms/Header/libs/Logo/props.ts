import { HTMLAttributes, ImgHTMLAttributes } from 'react';

import { ColorProps } from '../../../../../core/colors';

export type Props = HTMLAttributes<HTMLDivElement> &
  Pick<ImgHTMLAttributes<HTMLImageElement>, 'src' | 'alt'> & { readonly colors: ColorProps };
