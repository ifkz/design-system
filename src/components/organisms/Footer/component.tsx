import styled from '@emotion/styled';
import classNames from 'classnames';
import React, { FC } from 'react';

import { Container } from '../../atoms/Container';
import { Props } from './props';

const FooterBase: FC<Props> = ({ className, colors: _a, children, ...rest }: Props) => (
  <footer className={classNames(className, 'footer', 'socials')} {...rest}>
    <Container className={classNames('d-flex', 'align-items-center')}>{children}</Container>
  </footer>
);

export const Footer = styled(FooterBase)<Pick<Props, 'colors'>>`
  padding: 20px 0;
  background: rgb(10, 12, 25);
  color: #fff;
  .social {
    display: inline-block;
    height: 30px;
    margin: 5px;
  }
  .social__img {
    height: 100%;
    display: inline-block;
  }
  .social__links {
    padding: 0 10px;
    text-decoration: none;
    color: #fff;
    font-size: 15px;
    font-weight: 600;
    text-transform: uppercase;
    transition: color 0.2s, transform 0.2s;
  }
  .social__links:hover {
    color: ${({ colors }) => colors.accentBlue};
    transform: translateX(-10px);
  }
  .socials__partners--wrapper {
    flex: 1;
  }
  .container {
    display: flex;
    align-items: center;
  }
  .title {
    margin-bottom: 10px;
    font-size: 15px;
  }
`;
