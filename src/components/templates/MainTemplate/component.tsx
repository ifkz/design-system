import styled from '@emotion/styled';
import React, { FC, useState } from 'react';

import { Anchor } from '../../atoms/Anchor';
import { Butter } from '../../molecules/Butter';
import { LocaleSelect } from '../../molecules/LocaleSelect';
import { Navigation, NavigationItem } from '../../molecules/Navigation';
import { ThemeSwitch } from '../../molecules/ThemeSwitch';
import { Footer } from '../../organisms/Footer';
import { FooterAbout } from '../../organisms/FooterAbout';
import { FooterWrapper } from '../../organisms/FooterWrapper';
import { Header } from '../../organisms/Header';
import { Logo } from '../../organisms/Header/libs/Logo';
import { Props } from './props';

const MainBody = styled.div`
  flex: 1;
`;

const MainTemplateBase: FC<Props> = ({
  colors,
  footer,
  footerAbout,
  header,
  children,
  ...rest
}: Props) => {
  const [isNavActive, setIsNavActive] = useState(false);

  function toggleNav(): void {
    setIsNavActive(!isNavActive);
  }
  return (
    <div {...rest}>
      <Header colors={colors}>
        <Anchor href={header.logo.url}>
          <Logo colors={colors} src={header.logo.img} alt="Internet Freedom">
            Internet Freedom
            <br />
            <b>Kazakhstan</b>
          </Logo>
        </Anchor>
        <Butter colors={colors} onClick={toggleNav}>
          Menu
        </Butter>
        {isNavActive && (
          <>
            <button className="shade" onClick={toggleNav} />
            <Navigation>
              {header.links.map((item, i) => (
                <NavigationItem
                  colors={colors}
                  href={item.link}
                  className="navigation__link"
                  key={i}
                >
                  {item.text}
                </NavigationItem>
              ))}
              <ThemeSwitch colors={colors} isActive={true} className="mt-4">
                {header.theme}
              </ThemeSwitch>
              <LocaleSelect
                className="mt-3"
                languages={header.languages}
                locale={Object.keys(header.languages)[0]}
              />
            </Navigation>
          </>
        )}
      </Header>
      <MainBody>{children}</MainBody>
      <FooterWrapper>
        <FooterAbout title={footerAbout.title}>{footerAbout.content}</FooterAbout>
        <Footer colors={colors}>
          <div className="socials__partners--wrapper">
            {footer.links.map((link, i) => (
              <Anchor className="social__links" key={i} href={link.url}>
                {link.text}
              </Anchor>
            ))}
          </div>
          <div className="socials__info">
            <h3 className="title">{footer.title}</h3>
            <div className="socials-grid">
              {footer.socials.map((social, i) => (
                <div className="social" key={i}>
                  <Anchor href={social.url}>
                    <img src={social.img} alt="" className="social__img" />
                  </Anchor>
                </div>
              ))}
            </div>
          </div>
        </Footer>
      </FooterWrapper>
    </div>
  );
};

export const MainTemplate = styled(MainTemplateBase)<Pick<Props, 'colors'>>`
  display: flex;
  min-height: 100vh;
  flex-direction: column;
`;
