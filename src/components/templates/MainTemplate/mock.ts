export const header = {
  logo: {
    url: '/',
    img: 'https://ifkz.org/src/assets/logo.png',
  },
  theme: 'Theme',
  links: [
    { link: '/registry', text: 'Registry' },
    { link: '/news', text: 'Blog' },
    { link: '/cases', text: 'Cases' },
    { link: '/smi', text: 'Media' },
    { link: '/consult', text: 'Consult' },
    { link: '/contacts', text: 'Contacts' },
  ],
  languages: {
    ru: 'https://ifkz.org/src/assets/locales/ru.png',
    en: 'https://ifkz.org/src/assets/locales/en.png',
    kk: 'https://ifkz.org/src/assets/locales/kk.png',
  },
};

export const footer = {
  links: [
    {
      url: '/privacy',
      text: 'Privacy',
    },
    {
      url: '/socials',
      text: 'Socials',
    },
  ],
  title: 'Our social medias',
  socials: [
    {
      img: 'https://ifkz.org/src/assets/popular/instagram.png',
      url: 'https://instagram.com/internetfreedom_kz?igshid=g7nrax74t8te',
    },
    {
      img: 'https://ifkz.org/src/assets/popular/facebook.png',
      url: 'https://www.facebook.com/internetfreedomkz/',
    },
    {
      img: 'https://ifkz.org/src/assets/popular/telegram.png',
      url: 'https://t.me/InternetFreedomKZ',
    },
  ],
};

export const footerAbout = {
  title: 'О проекте',
  content:
    'Проект разработан при финансовой поддержке Фонда Сорос-Казахстан. Содержание любой публикации отражает точку зрения автора/ов, которая не обязательно совпадает с точкой зрения Фонда Сорос-Казахстан.',
};
